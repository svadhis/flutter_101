import 'package:flutter/material.dart';

class GenericFloatingButton extends StatelessWidget {
  /// Bouton flottant par défaut. L'argument `action` est requis
  const GenericFloatingButton(this.icon, {this.tooltip, @required this.action, Key key}) : super(key: key);

  final IconData icon;
  final String tooltip;
  final Function action;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: action,
      tooltip: tooltip,
      child: Icon(icon),
    );
  }
}