import 'package:flutter/material.dart';

class GenericFlatButton extends StatelessWidget {
  const GenericFlatButton(this.text, {@required this.action, Key key}) : super(key: key);

  final String text;
  final Function action;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: action,
      padding: EdgeInsets.all(16),
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child: Text(text),
    );
  }
}