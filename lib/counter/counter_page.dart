import 'package:flutter/material.dart';
import 'package:flutter_101/counter/counter_state.dart';
import 'package:flutter_101/counter/widgets/generic_flat_button.dart';
import 'package:flutter_101/counter/widgets/generic_floating_button.dart';
import 'package:provider/provider.dart';

class CounterPage extends StatelessWidget {
  const CounterPage({this.title, Key key}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Consumer<CounterState>(
      builder: (context, state, widget) {
        return Scaffold(
          appBar: AppBar(
            title: Text(title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'You have pushed the button this many times:',
                ),
                Text(
                  '${state.counter}',
                  style: Theme.of(context).textTheme.headline4,
                ),
                if (state.counter > 0) Container(
                  margin: EdgeInsets.only(top: 12),
                  child: GenericFlatButton(
                    'Reset',
                    action: state.resetCounter,
                  ),
                )
              ],
            ),
          ),
          floatingActionButton: GenericFloatingButton(
            Icons.add, 
            tooltip: 'Increment', 
            action: state.incrementCounter
          ),
        );
      }
    );
  }
}