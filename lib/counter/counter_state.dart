import 'package:flutter/cupertino.dart';

class CounterState extends ChangeNotifier {

  int counter = 0;

  void incrementCounter() {
    counter++;
    notifyListeners();
  }

  void resetCounter() {
    counter = 0;
    notifyListeners();
  }

}