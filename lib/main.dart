import 'package:flutter/material.dart';
import 'package:flutter_101/counter/counter_page.dart';
import 'package:flutter_101/counter/counter_state.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CounterState(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter 101',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: CounterPage(title: 'Flutter 101'),
      )
    );
  }
}
